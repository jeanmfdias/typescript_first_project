export class Negotiation {
    private _createdAt: Date;
    private _value: number;
    private _quantity: number;

    constructor(createdAt: Date, value: number, quantity: number) {
        this._createdAt = createdAt;
        this._value = value;
        this._quantity = quantity;
    }

    get createdAt(): Date {
        return this._createdAt;
    }

    get value(): number {
        return this._value;
    }

    get quantity(): number {
        return this._quantity;
    }

    get volume(): number {
        return this._quantity * this._value;
    }
}