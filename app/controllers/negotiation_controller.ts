import { Negotiation } from "../models/negotiation.js";
import { Negotiations } from "../models/negotiations.js";

export class NegotiationController {
    private _inputDate: HTMLInputElement;
    private _inputQuantity: HTMLInputElement;
    private _inputValue: HTMLInputElement;
    private _negotiations = new Negotiations();

    constructor() {
        this._inputDate = document.querySelector('#date');
        this._inputQuantity = document.querySelector('#quantity');
        this._inputValue = document.querySelector('#value');
    }

    create(): Negotiation {
        const exp = /-/g;
        const date = new Date(this._inputDate.value.replace(exp, ','));
        const quantity = parseInt(this._inputQuantity.value);
        const value = parseFloat(this._inputValue.value);

        return new Negotiation(date, value, quantity);
    }

    store(): void {
        const negotiation = this.create();

        this._negotiations.add(negotiation);
        console.log(this._negotiations.list());

        this.clearForm();
    }

    clearForm(): void {
        this._inputDate.value = '';
        this._inputQuantity.value = '';
        this._inputValue.value = '';
        this._inputDate.focus();
    }
}